#include <ros/ros.h>
#include <ros/package.h>

#include <rosbag/bag.h>
#include <rosbag/view.h>

#include <cv_bridge/cv_bridge.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <sensor_msgs/Image.h>
#include <typeinfo>

int main(int argc, char **argv){
	
	if (argc != 3){
		std::cout<<" usage image_from_bag bag_file image_prefix"<<std::endl;
		return 0;
	}
	
	std::string g_image_topic = "/vertical_stereo/right/image_raw";
	std::string g_bag_name = argv[1];
	std::string prefix = argv[2];
	std::string target_dir = 
		"/home/arul/mbzirc/wrench_perception/data/haar_data/data_from_bag";
	
	rosbag::Bag bag;
	bag.open(g_bag_name,rosbag::bagmode::Read);
	std::cout<<"Bag file name "<<bag.getFileName()<<" size "<< bag.getSize() <<std::endl;
		
	u_int count = 0;
	rosbag::View view(bag, rosbag::TopicQuery(g_image_topic));
	cv_bridge::CvImagePtr img_ptr;
	
	for ( rosbag::View::iterator it = view.begin(); it != view.end(); ++it ){
		rosbag::MessageInstance & m = *it;
		sensor_msgs::ImageConstPtr msg = m.instantiate<sensor_msgs::Image>();
		try {
			img_ptr = cv_bridge::toCvCopy( msg, msg->encoding );
		} catch (cv_bridge::Exception& e) {
			ROS_ERROR("cv_bridge exception: %s", e.what());
		}
		cv::Mat cv_img = img_ptr->image;
		
		if (count % 15 == 0) {
			char *filename;
			asprintf(&filename, "%s/%s_img_%u.jpg", target_dir.c_str(),prefix.c_str(), count);
			cv::imwrite(filename, cv_img);
			free(filename);
		}
		count++;
	}
	std::cout<<"Done"<<std::endl;
	return 0;
}

