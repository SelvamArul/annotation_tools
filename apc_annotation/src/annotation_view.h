// Annotation view / editor
// Author: Max Schwarz <max.schwarz@uni-bonn.de>

#ifndef ANNOTATION_VIEW_H
#define ANNOTATION_VIEW_H

#include "item_model.h"

#include <QtGui/QWidget>

#include <opencv2/core/core.hpp>

class AnnotationModel;
class QItemSelectionModel;
class QUndoStack;

class AnnotationView : public QWidget
{
Q_OBJECT
public:
	AnnotationView(QWidget* parent = 0);
	virtual ~AnnotationView();

	void setUndoStack(QUndoStack* undo);

	void setBackground(const QPixmap& pixmap);

	inline const QPixmap& background() const
	{ return m_background; }

	void setModel(AnnotationModel* model);
	void setSelectionModel(QItemSelectionModel* model);

	void addPoint(const cv::Point& point);
	void removeLastPoint();

public Q_SLOTS:
	void abortCurrentPolygon();
	void switchMode();

protected:
	void mousePressEvent(QMouseEvent* event) override;
	void mouseMoveEvent(QMouseEvent* event) override;
	void mouseReleaseEvent(QMouseEvent* event) override;
	void wheelEvent(QWheelEvent* event) override;

	void paintEvent(QPaintEvent *) override;

private:
	void updateTransform();

	QPoint mapPoint(const cv::Point& point) const;

	QUndoStack* m_undo = 0;

	QPixmap m_background;
	AnnotationModel* m_model = 0;
	QItemSelectionModel* m_selectionModel = 0;

	std::vector<cv::Point> m_currentPolygon;
	cv::Point m_centerPoint;

	ItemModel m_itemModel;

	QTransform m_panZoom;
	QTransform m_transform;

	QPoint m_lastPan;
	
	bool m_isCenterPoint;
	bool m_isBBMode; // bounding box or circle mode
		
	struct bounding_box {
		private: int c_x, c_y;
		public: int h, w;
		bounding_box(): h(175), w(250) {};
		int get_bb_x() {
			return c_x - w / 2;
		}
		int get_bb_y() {
			return c_y - h / 2;
		}
		int get_center_x(){
			return c_x;
		}
		int get_center_y(){
			return c_y;
		}
		int set_center_x(int x){
			c_x = x;
		}
		int set_center_y(int y){
			c_y = y;
		}
	};
	
	bounding_box m_ellipse;
	bounding_box m_rect;
};

#endif
