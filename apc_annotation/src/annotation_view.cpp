// Annotation view / editor
// Author: Max Schwarz <max.schwarz@uni-bonn.de>

#include "annotation_view.h"
#include "annotation_model.h"
#include "item_dialog.h"

#include <QtGui/QPainter>
#include <QtGui/QMouseEvent>
#include <QtGui/QInputDialog>
#include <QtGui/QItemSelectionModel>
#include <QtGui/QUndoStack>
#include <QApplication>

#include <QtCore/QDebug>
#include <iostream>
namespace undo
{
	class AddPointCommand : public QUndoCommand
	{
	public:
		AddPointCommand(AnnotationView* view, const cv::Point& point)
		 : QUndoCommand("add point")
		 , m_view(view)
		 , m_point(point)
		{
		}

		virtual void redo() override
		{
			m_view->addPoint(m_point);
		}

		virtual void undo() override
		{
			m_view->removeLastPoint();
		}
	private:
		AnnotationView* m_view;
		cv::Point m_point;
	};
}

AnnotationView::AnnotationView(QWidget* parent)
 : QWidget(parent)
{
	m_isCenterPoint = true;
	setMouseTracking(true);
	m_isBBMode = true;
	m_ellipse.h = 200;
	m_ellipse.w = 200;
}

AnnotationView::~AnnotationView()
{
}

void AnnotationView::setUndoStack(QUndoStack* undo)
{
	m_undo = undo;
}

void AnnotationView::setBackground(const QPixmap& pixmap)
{
	m_background = pixmap;
	m_panZoom.reset();
	m_currentPolygon.clear();
	m_undo->clear();

	updateTransform();
	update();
}

void AnnotationView::setModel(AnnotationModel* model)
{
	m_model = model;

	connect(m_model, SIGNAL(modelReset()), SLOT(update()));
	connect(m_model, SIGNAL(rowsInserted(QModelIndex,int,int)), SLOT(update()));
	connect(m_model, SIGNAL(rowsRemoved(QModelIndex,int,int)), SLOT(update()));
}

void AnnotationView::setSelectionModel(QItemSelectionModel* model)
{
	m_selectionModel = model;

	connect(m_selectionModel, SIGNAL(currentChanged(QModelIndex,QModelIndex)), SLOT(update()));
}

void AnnotationView::updateTransform()
{
	QRect pixmapRect = m_background.rect();

	float scale = std::min(
		(float)width()/pixmapRect.width(),
		(float)height()/pixmapRect.height()
	);

	// Apply pan/zoom initially
	m_transform = m_panZoom;

	// Rotate everything by 180°
	m_transform.translate(width()/2, height()/2);
	//m_transform.rotate(180);
	m_transform.translate(-width()/2, -height()/2);

	// move pixmap to center
	m_transform.translate(
		(width() - scale * pixmapRect.width())/2,
		(height() - scale * pixmapRect.height())/2
	);

	// scale s.t. pixmap fits
	m_transform.scale(scale, scale);
}

QPoint AnnotationView::mapPoint(const cv::Point& point) const
{
	return QPoint(point.x, point.y);
}

void AnnotationView::paintEvent(QPaintEvent*)
{
	
	QPainter painter(this);

	updateTransform(); // FIXME: move to resizeEvent

	painter.setTransform(m_transform);
	painter.drawPixmap(m_background.rect(), m_background);

	for(int i = 0; i < m_model->rowCount(); ++i)
	{
		const AnnotationItem& item = m_model->item(i);
		QModelIndex index = m_model->index(i, 0);

		QPolygon polygon;
		for(auto& point : item.polygon)
			polygon.append(mapPoint(point));


		bool current = m_selectionModel && (m_selectionModel->currentIndex() == index);

		QPen pen;
		pen.setCosmetic(true);
		pen.setColor(current ? Qt::green : Qt::red);
		pen.setWidth(3);
		painter.setPen(pen);

		if(item.name != "box")
		{
			QBrush brush;
			brush.setColor(current ? QColor(0, 255, 0, 80) : QColor(255, 0, 0, 80));
			brush.setStyle(Qt::BDiagPattern);
			painter.setBrush(brush);
		}
		else
		{
			painter.setBrush(QBrush());
		}

		painter.drawPolygon(polygon);
	}

	QPen pen;
	pen.setCosmetic(true);
	pen.setColor(Qt::red);
	pen.setWidth(5);
	painter.setPen(pen);
		
	if(m_isBBMode){
		cv::Point cvPoint(m_rect.get_center_x(), m_rect.get_center_y());
		QPoint center = mapPoint(cvPoint);
		painter.drawPoint(center);
		pen.setColor(Qt::yellow);
		pen.setWidth(3);
		painter.setPen(pen);
		painter.drawRect(m_rect.get_bb_x(), m_rect.get_bb_y(), m_rect.w, m_rect.h);
	}
		
	if (!m_isBBMode){
		cv::Point cvPoint(m_ellipse.get_center_x(), m_ellipse.get_center_y());
		QPoint center = mapPoint(cvPoint);
		painter.drawPoint(center);
		pen.setColor(Qt::yellow);
		pen.setWidth(3);
		painter.setPen(pen);
		painter.drawRect(m_ellipse.get_bb_x(), m_ellipse.get_bb_y(), m_ellipse.w, m_ellipse.h);
	}
}

void AnnotationView::mousePressEvent(QMouseEvent* event)
{
	m_lastPan = event->pos();
	
	// only for LeftButton
// 	if(event->buttons() == Qt::LeftButton)
// 		m_isCenterPoint = !m_isCenterPoint;
	
	
	event->accept();
}

void AnnotationView::mouseMoveEvent(QMouseEvent* event)
{
	if(event->buttons() & Qt::RightButton)
	{
		QPoint delta = event->pos() - m_lastPan;

		m_panZoom = m_panZoom * QTransform::fromTranslate(delta.x(), delta.y());
		updateTransform();
		update();
	}
	
	QPoint mousePos = event->pos();

	mousePos = m_transform.inverted().map(mousePos);

	// If outside of pixmap, clamp
	if(mousePos.x() < 0)
		mousePos.setX(0);
	if(mousePos.y() < 0)
		mousePos.setY(0);
	if(mousePos.x() >= m_background.width())
		mousePos.setX(m_background.width()-1);
	if(mousePos.y() >= m_background.height())
		mousePos.setY(m_background.height()-1);
	
	if (m_isCenterPoint && m_isBBMode) {
	
// 		m_currentPolygon.clear();
// 		// add 4 corners to m_currentPolygon
// 		int r_width, r_height;
// 		r_width = mousePos.x() - m_centerPoint.x;
// 		r_height = mousePos.y() - m_centerPoint.y;
// // 			// topRight
// 		m_currentPolygon.emplace_back(cv::Point(mousePos.x(), mousePos.y()));
// 		// topLeft
// 		m_currentPolygon.emplace_back(cv::Point(mousePos.x() - 2 * r_width, mousePos.y()));
// 		// bottomLeft
// 		m_currentPolygon.emplace_back(cv::Point(mousePos.x() - 2 * r_width,  mousePos.y() - 2 * r_height));
// 		// bottomRight
// 		m_currentPolygon.emplace_back(cv::Point(mousePos.x(),  mousePos.y() - 2 * r_height));
		
		m_rect.set_center_x( mousePos.x() );
		m_rect.set_center_y( mousePos.y() );

	} else if (!m_isBBMode) {
		m_ellipse.set_center_x( mousePos.x() );
		m_ellipse.set_center_y( mousePos.y() );
	}
	
	update();
	m_lastPan = event->pos();
	
	
}

void AnnotationView::addPoint(const cv::Point& point)
{
	m_currentPolygon.push_back(point);
	update();
}

void AnnotationView::removeLastPoint()
{
	m_currentPolygon.pop_back();
	update();
}

void AnnotationView::mouseReleaseEvent(QMouseEvent* event)
{
	if(event->button() != Qt::LeftButton)
		return;

	QPoint mousePos = event->pos();

	mousePos = m_transform.inverted().map(mousePos);

	// If outside of pixmap, clamp
	if(mousePos.x() < 0)
		mousePos.setX(0);
	if(mousePos.y() < 0)
		mousePos.setY(0);
	if(mousePos.x() >= m_background.width())
		mousePos.setX(m_background.width()-1);
	if(mousePos.y() >= m_background.height())
		mousePos.setY(m_background.height()-1);

	cv::Point cvPoint(mousePos.x(), mousePos.y());

	if (m_isBBMode) {
		m_currentPolygon.clear();
		m_currentPolygon.emplace_back(cv::Point(m_rect.get_bb_x(), m_rect.get_bb_y()));
		m_currentPolygon.emplace_back(cv::Point(m_rect.get_bb_x() ,m_rect.get_bb_y() + m_rect.h));
		m_currentPolygon.emplace_back(cv::Point(m_rect.get_bb_x() + m_rect.w, m_rect.get_bb_y() + m_rect.h));
		m_currentPolygon.emplace_back(cv::Point(m_rect.get_bb_x() + m_rect.w, m_rect.get_bb_y()));
		ItemDialog dlg(&m_itemModel, this);
			
		m_undo->push(new undo::AddPointCommand(this, cv::Point(m_rect.get_bb_x(), m_rect.get_bb_y()))); // top right
		if(dlg.exec() == QDialog::Accepted)
		{
			m_model->addItem(dlg.name(), m_currentPolygon);
		}

		m_currentPolygon.clear();

		// FIXME: Undo across adding/removing items?
		m_undo->clear();

	} else {
		// convert m_ellipse to Bounding box
		m_currentPolygon.clear();
		m_currentPolygon.emplace_back(cv::Point(m_ellipse.get_bb_x(), m_ellipse.get_bb_y()));
		m_currentPolygon.emplace_back(cv::Point(m_ellipse.get_bb_x() ,m_ellipse.get_bb_y() + m_ellipse.h));
		m_currentPolygon.emplace_back(cv::Point(m_ellipse.get_bb_x() + m_ellipse.w, m_ellipse.get_bb_y() + m_ellipse.h));
		m_currentPolygon.emplace_back(cv::Point(m_ellipse.get_bb_x() + m_ellipse.w, m_ellipse.get_bb_y()));
		ItemDialog dlg(&m_itemModel, this);
			
		m_undo->push(new undo::AddPointCommand(this, cv::Point(m_ellipse.get_bb_x(), m_ellipse.get_bb_y()))); // top right
		//TODO compute and add other points
		if(dlg.exec() == QDialog::Accepted)
		{
			m_model->addItem(dlg.name(), m_currentPolygon);
		}

		m_currentPolygon.clear();

		// FIXME: Undo across adding/removing items?
		m_undo->clear();
	}

	update();
	event->accept();
}

void AnnotationView::wheelEvent(QWheelEvent* event)
{
	if (QApplication::queryKeyboardModifiers() != Qt::CTRL) {
		float scale = std::pow(1.1f, float(event->delta()) / 120);

		QTransform zoom;

		zoom.translate(event->pos().x(), event->pos().y());
		zoom.scale(scale, scale);
		zoom.translate(-event->pos().x(), -event->pos().y());

		m_panZoom = m_panZoom * zoom;

		updateTransform();
	}
	else if (!m_isBBMode){
		m_ellipse.h += event->delta() / 15;
		m_ellipse.w += event->delta() / 15;
	}
	update();
}

void AnnotationView::abortCurrentPolygon()
{
	m_currentPolygon.clear();
	m_undo->clear();
	update();
}

void AnnotationView::switchMode(){
	m_isBBMode = !m_isBBMode;
}
