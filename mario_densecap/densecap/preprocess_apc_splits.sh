#!/bin/bash

dataset=$1
depth=feature_hhav.png
variant=depth-hhav

for split in data/split*.txt; do
	split_name=$(basename $split)
	echo "Processing split $split_name"

	python preprocess_apc.py --dataset $dataset --h5_output data/apc-$variant-split-$split_name.h5 --json_output data/apc-$variant-split-$split_name.json --test_set $split --val_split 0 --depth $depth
done
