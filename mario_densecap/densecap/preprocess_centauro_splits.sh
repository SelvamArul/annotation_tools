#!/bin/bash

dataset=$1
depth=''
variant=""

for split in $dataset/splits/*fbb*.txt; do
	split_name=$(basename $split)
	echo "Processing split $split_name"

	python preprocess_centauro.py --dataset $dataset --h5_output data/centauro$variant-split-$split_name.h5 --json_output data/centauro$variant-split-$split_name.json --test_set $split --depth "$depth"
done
