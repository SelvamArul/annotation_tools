// Author: Arul Selvam <arulselvam@uni-bonn.de>

#ifndef APC_DENSECAP_H
#define APC_DENSECAP_H

#include <memory>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>


#include <Eigen/Dense>

#include <map>

namespace mario_densecap
{

class MARIODenseCapPrivate;

class MARIODenseCap
{

public:

	struct Result
	{
		//! Position in 2D image
		Eigen::Vector2f center;

		//! Size in 2D image
		Eigen::Vector2f size;

		//! Classifier response (hint: if negative, discard!)
		float response;

		cv::Mat_<float> likelihood;
	};

	typedef std::map<std::string, Result> MultiResult;

	MARIODenseCap();
	virtual ~MARIODenseCap();

	void initialize(const std::string& datasetPath, int gpu = 0);

	void train(const std::vector<std::string>& presentObjects, const std::string& target);
	
	void runPrediction(cv::Mat_<cv::Vec3b>& cvImage);
	
	
	int test_interface(); // remove this once the c++ interface is stabe TODO

private:
	std::unique_ptr<MARIODenseCapPrivate> m_d;
};

}

#endif
