
-- FIXME NOT STABLE
-- FIXME Copy comments from apc.lua

require 'torch'
require 'nn'
require 'image'

require 'densecap.DetectionModel'
require 'densecap.DataLoader'
local utils = require 'densecap.utils'
local box_utils = require 'densecap.box_utils'
local vis_utils = require 'densecap.vis_utils'



local densecapCudaDevice = 0
local dtype, use_cudnn = nil, nil

function load_model(checkpoint_file, gpu)
  densecapCudaDevice = gpu
  dtype, use_cudnn = utils.setup_gpus(densecapCudaDevice, 1) -- FIXME: enable cudnn!

  print("CUDA device during load: ", cutorch.getDevice())

  local checkpoint = torch.load(checkpoint_file)
  model = checkpoint.model
  model:convert(dtype, use_cudnn)
  model:setTestArgs{    -- FIXME: some const values?
    rpn_nms_thresh = 0.7,
    final_nms_thresh = 0.3,
    num_proposals = 1000,
  }
  model:evaluate()
end


function run_image(img_in)

  -- Load, resize, and preprocess image

  img = image.scale(img_in, 720):float()
  local H, W = img:size(2), img:size(3)
  local img_caffe = img:view(1, 3, H, W)
  local vgg_mean = torch.FloatTensor{103.939, 116.779, 123.68}
  vgg_mean = vgg_mean:view(1, 3, 1, 1):expand(1, 3, H, W)
  img_caffe:add(-1, vgg_mean)

  label_mask = nil
  
  -- Run the model forward
  local boxes, scores, classes, captions, class_scores = model:forward_test(img_caffe:type(dtype), label_mask)

  local boxes_xywh
  if boxes:nDimension() ~= 0 then
    boxes_xywh = box_utils.xcycwh_to_xywh(boxes)
  else
    boxes_xywh = torch.Tensor()
  end
  boxes = boxes:float()
  boxes_xywh = boxes_xywh:float()
  scores = scores:float()
  classes = classes:long()
  class_scores = class_scores:float()
  
  local out = {
     boxes = boxes_xywh,
     boxes_xcycwh = boxes,
     scores = scores,
     classes = classes,
     class_scores = class_scores
  }

  return out
end
