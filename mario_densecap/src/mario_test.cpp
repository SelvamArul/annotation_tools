// test file for mario_denscap library
// remove this when the c++ interface is stable

// Author: Arul Selvam <arulselvam@uni-bonn.de>


#include <mario_densecap/mario_densecap.h>
#include <iostream>
#include <assert.h> 


#include <ros/console.h>
#include <ros/package.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

namespace mdns = mario_densecap;
int main(){
	if( ros::console::set_logger_level(
		ROSCONSOLE_DEFAULT_NAME, 
		ros::console::levels::Debug) ) 
	{
		ros::console::notifyLoggerLevelsChanged();
	}
	
	
	mdns::MARIODenseCap md;
	md.initialize("dummy path", 1);
	cv::Mat_<cv::Vec3b> image;
	image = cv::imread(ros::package::getPath("mario_densecap") + "/src/gray.png", CV_LOAD_IMAGE_COLOR);
// 	cv::imshow("gray", image);
// 	cv::waitKey(0);
	
	ROS_DEBUG("calling interface");
	md.runPrediction(image);
	assert(md.test_interface() == 1);
	std::cout<<"=================== DONE ==================="<<std::endl;
	return 0;
}
