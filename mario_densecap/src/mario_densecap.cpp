// Author: Arul Selvam <arulselvam@uni-bonn.de>

#include <mario_densecap/mario_densecap.h>

#include <ros/console.h>
#include <ros/package.h>

#include <algorithm>

#ifndef APC_DENSECAP_MOCK

extern "C"
{
#  include <lualib.h>
#  include <lauxlib.h>
#  include <luaT.h>
}

#  include <TH/THTensor.h>

#endif


namespace mario_densecap
{

class MARIODenseCapPrivate
{
public:
#ifndef APC_DENSECAP_MOCK
	typedef Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> ResultMatrix;
	typedef Eigen::Matrix<long, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> ResultMatrixLong;
	
	void callLUA(
		const cv::Mat_<cv::Vec3b>& cvImage,
		ResultMatrix* boxes,
		ResultMatrix* boxes_xcycwh,
		ResultMatrix* scores,
		ResultMatrixLong* classes,
		ResultMatrix* class_scores
	);

	std::string lua_traceback()
	{
		luaL_traceback(L, L, NULL, 0);
		return lua_tostring(L, -1);
	}

	static void handleTorchError(const char* msg, void* data)
	{
		MARIODenseCapPrivate* d = reinterpret_cast<MARIODenseCapPrivate*>(data);

		ROS_ERROR("Torch error: '%s'\n", msg);
		ROS_ERROR("LUA traceback:\n%s", d->lua_traceback().c_str());
		std::abort();
	}

	static void handleTorchArgError(int idx, const char* msg, void* data)
	{
		MARIODenseCapPrivate* d = reinterpret_cast<MARIODenseCapPrivate*>(data);

		ROS_ERROR("Torch argument %d error: '%s'\n", idx, msg);
		ROS_ERROR("LUA traceback:\n%s", d->lua_traceback().c_str());
		std::abort();
	}

	void prepareLUA()
	{
		THSetErrorHandler(&handleTorchError, this);
		THSetArgErrorHandler(&handleTorchArgError, this);
	}

	lua_State* L = 0;

#endif

};

#ifndef APC_DENSECAP_MOCK

void MARIODenseCapPrivate::callLUA(
	const cv::Mat_<cv::Vec3b>& cvImage,
	ResultMatrix* boxes, ResultMatrix* boxes_xcycwh,
	ResultMatrix* scores, ResultMatrixLong* classes, ResultMatrix* class_scores
){
	prepareLUA();
	
	THFloatTensor* tensor = THFloatTensor_newWithSize3d(
		3, cvImage.rows, cvImage.cols
	);
	float* rgbData = THFloatTensor_data(tensor);
	
	{
		int CHANNEL_STRIDE = cvImage.rows * cvImage.cols;
		
		for(unsigned int y = 0; y < cvImage.rows; ++y)
		{
			for(unsigned int x = 0; x < cvImage.cols; ++x)
			{
				auto& p = cvImage(y,x);
				rgbData[0 * CHANNEL_STRIDE + y * cvImage.cols + x] = p[0];
				rgbData[1 * CHANNEL_STRIDE + y * cvImage.cols + x] = p[1];
				rgbData[2 * CHANNEL_STRIDE + y * cvImage.cols + x] = p[2];
			}
		}
	}
	
	// Call LUA code
	{
		lua_getglobal(L, "run_image");

		luaT_pushudata(L, tensor, "torch.FloatTensor");

		if(lua_pcall(L, 1, 1, 0) != 0)
		{
			ROS_ERROR("LUA error:\n%s", lua_tostring(L, -1));
			throw std::runtime_error("LUA error");
		}

		int outIdx = lua_gettop(L);

		THFloatTensor* thboxes = (THFloatTensor*)luaT_getfieldcheckudata(L, outIdx, "boxes", "torch.FloatTensor"); 
		THFloatTensor* thboxes_xcycwh = (THFloatTensor*)luaT_getfieldcheckudata(L, outIdx, "boxes_xcycwh", "torch.FloatTensor");
		THFloatTensor* thscores = (THFloatTensor*)luaT_getfieldcheckudata(L, outIdx, "scores", "torch.FloatTensor");
		THLongTensor* thclasses = (THLongTensor*)luaT_getfieldcheckudata(L, outIdx, "classes", "torch.LongTensor");
		THFloatTensor* thclass_scores = (THFloatTensor*)luaT_getfieldcheckudata(L, outIdx, "class_scores", "torch.FloatTensor");

		ROS_INFO("Got %ldx%ld box data and %ldx%ld class_scores",
			THFloatTensor_size(thboxes, 0), THFloatTensor_size(thboxes, 1),
			THFloatTensor_size(thclass_scores, 0), THFloatTensor_size(thclass_scores, 1)
 		);
		
		ROS_INFO("Got %ldx%ld classes ",
			THLongTensor_size(thclasses, 0), THLongTensor_size(thclasses, 1));

		*boxes = ResultMatrix::Map(
			THFloatTensor_data(thboxes),
			THFloatTensor_size(thboxes, 0), THFloatTensor_size(thboxes, 1)
		);
		*boxes_xcycwh = ResultMatrix::Map(
			THFloatTensor_data(thboxes_xcycwh),
			THFloatTensor_size(thboxes_xcycwh, 0), THFloatTensor_size(thboxes_xcycwh, 1)
		);
		*scores = ResultMatrix::Map(
			THFloatTensor_data(thscores),
			THFloatTensor_size(thscores, 0), THFloatTensor_size(thscores, 1)
		);
		*classes = ResultMatrixLong::Map(
			THLongTensor_data(thclasses),
			THLongTensor_size(thclasses, 0), THLongTensor_size(thclasses, 1)
		);
		*class_scores = ResultMatrix::Map(
			THFloatTensor_data(thclass_scores),
			THFloatTensor_size(thclass_scores, 0), THFloatTensor_size(thclass_scores, 1)
		);

		lua_settop(L, outIdx-1);

		lua_gc(L, LUA_GCCOLLECT, 0);
	}
}


MARIODenseCap::MARIODenseCap()
 : m_d(new MARIODenseCapPrivate)
{
}

MARIODenseCap::~MARIODenseCap()
{
#ifndef APC_DENSECAP_MOCK
	THSetErrorHandler(0, 0);
	THSetArgErrorHandler(0, 0);

	if(m_d->L)
		lua_close(m_d->L);
#endif
}

void MARIODenseCap::initialize(const std::string& datasetPath, int gpu)
	{
		
		if( ros::console::set_logger_level(
		ROSCONSOLE_DEFAULT_NAME, 
		ros::console::levels::Debug) ) 
		{
			ros::console::notifyLoggerLevelsChanged();
		}
		
		#ifndef APC_DENSECAP_MOCK
		// Create LUA interpreter
		m_d->L = luaL_newstate();
		if(!m_d->L)
			throw std::runtime_error("Could not create LUA instance");

		// Load LUA standard libraries
		luaL_openlibs(m_d->L);

		m_d->prepareLUA();

		// Modify LUA path to include densecap
		{
			lua_getglobal(m_d->L, "package");
			lua_getfield(m_d->L, -1, "path");

			std::string path = lua_tostring(m_d->L, -1);
			path += ";" + ros::package::getPath("mario_densecap") + "/densecap/?.lua";

			lua_pop(m_d->L, 1);

			lua_pushstring(m_d->L, path.c_str());
			lua_setfield(m_d->L, -2, "path");

			lua_pop(m_d->L, 1);
		}
		std::cout<<" LUA path setting done "<<std::endl;
		ROS_DEBUG("Running apc.lua...");
		std::string scriptFile = ros::package::getPath("mario_densecap") + "/lua/mario.lua";

		if(luaL_dofile(m_d->L, scriptFile.c_str()) != 0)
		{
			ROS_ERROR("Could not execute '%s'", scriptFile.c_str());
			ROS_ERROR("LUA error:\n%s", lua_tostring(m_d->L, -1));
			throw std::runtime_error("Could not execute LUA script");
		}
		ROS_DEBUG( " mario.lua file loaded ");
		ROS_DEBUG("Loading pretrained model...");
		{
			lua_getglobal(m_d->L, "load_model");

			std::string model = ros::package::getPath("mario_densecap") +
				"/densecap/data/models/densecap/centauro-checkpoint-fbb_split_test.txt.t7";
			lua_pushstring(m_d->L, model.c_str());
			lua_pushinteger(m_d->L, gpu);

			if(lua_pcall(m_d->L, 2, 0, 0) != 0)
			{
				ROS_ERROR("LUA error:\n%s", lua_tostring(m_d->L, -1));
				throw std::runtime_error("Could not load model");
			}
		}
		ROS_DEBUG("Pretrained model loaded...");
		#endif
	}
	
	void MARIODenseCap::runPrediction(cv::Mat_<cv::Vec3b>& cvImage){
		
		MARIODenseCapPrivate::ResultMatrix boxes;
		MARIODenseCapPrivate::ResultMatrix scores;
		MARIODenseCapPrivate::ResultMatrixLong classes;
		MARIODenseCapPrivate::ResultMatrix class_scores;
		MARIODenseCapPrivate::ResultMatrix boxes_xcycwh;
		
		m_d->callLUA(cvImage, &boxes, &boxes_xcycwh, &scores, &classes, &class_scores);
		ROS_INFO("Boxes   Eigen: %ldx%ld", boxes.rows(), boxes.cols());
		
		// convert to image space
		boxes = (1920.0/720.0)*boxes;
		
		// sort the Predictions based on scores
		std::vector<int> head_indexes(boxes.rows());
		std::iota(head_indexes.begin(), head_indexes.end(), 0);
		
		std::sort(head_indexes.begin(), head_indexes.end(), [&](int a, int b){
			return class_scores(a, 3) > class_scores(b, 3);
		});
		
		std::vector<int> mouth_indexes(boxes.rows());
		std::iota(mouth_indexes.begin(), mouth_indexes.end(), 0);
		
		std::sort(mouth_indexes.begin(), mouth_indexes.end(), [&](int a, int b){
			return class_scores(a, 1) + class_scores(a, 2) > class_scores(b, 1) + class_scores(b, 2);
		});
		
		
		
		
		// visualize Prediction
		{
			cv::Mat_<cv::Vec3b> visImage = cvImage.clone();
			
			for (u_int i = 0; i < 6; ++i) {
				int idx = head_indexes[i];
				
				ROS_DEBUG_STREAM(" box " << boxes.row(idx) 
					<< " classes " <<classes.row(idx)
					<< " scores " <<scores.row(idx));
				
				cv::rectangle(visImage, cv::Point(boxes(idx,0) , boxes(idx,1)), 
							  cv::Point(boxes(idx,0) + boxes(idx,2) , boxes(idx,1)+ boxes(idx,3)),
							  cv::Scalar(255,0,0), 2
 							);
				
				idx = mouth_indexes[i];
				cv::rectangle(visImage, cv::Point(boxes(idx,0) , boxes(idx,1)), 
							  cv::Point(boxes(idx,0) + boxes(idx,2) , boxes(idx,1)+ boxes(idx,3)),
							  cv::Scalar(0,0,255), 2
 							);
			}
			
			cv::imwrite(ros::package::getPath("mario_densecap") + "/src/result.png", visImage);
			ROS_DEBUG_STREAM("Result image written ");
			
		}
	}

	
	int MARIODenseCap::test_interface(){
		
		
		std::cout<<" ************ MARIODenseCap test interface returning ************" <<std::endl;
		return 1;
	}
#endif
}// end namespace mario_densecap
